import { Injectable } from "@angular/core";

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
}

const MENUITEMS = [
  //{ state: "dashboard", name: "Dashboard", type: "link", icon: "av_timer" },

  { state: "payment-management", type: "link", name: "Payment", icon: "event_available" },
  { state: "subscription-management", type: "link", name: "Subscription", icon: "report" }
];

@Injectable()
export class MenuItems {
  getMenuitem(): Menu[] {
    return MENUITEMS;
  }
}
