import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PaymentManagementComponent } from "../payment-management/payment-management.component";
import { ViewPaymentComponent } from "./view-payment/view-payment.component";

const routes: Routes = [
  {
    path: "",
    component: PaymentManagementComponent,
    children: [
      { path: "", redirectTo: "/main/payment-management/view-payment", pathMatch: "full" },
      { path: "view-payment", component: ViewPaymentComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentManagementRoutingModule {}
