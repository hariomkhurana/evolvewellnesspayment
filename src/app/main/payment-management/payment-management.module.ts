import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { PaymentManagementRoutingModule } from "./payment-management-routing.module";
import { PaymentManagementComponent } from "./payment-management.component";
import { ViewPaymentComponent } from "./view-payment/view-payment.component";
import { SharedModule } from "../../shared/shared.module";
import { PaymentMgmtService } from "./payment.service";
import { NgxStripeModule } from "ngx-stripe";

@NgModule({
  declarations: [PaymentManagementComponent, ViewPaymentComponent],
  imports: [
    CommonModule,
    PaymentManagementRoutingModule,
    SharedModule,
    NgxStripeModule.forRoot("pk_test_51I9qsTBIVTngAwh3LQkj9DHNc3kIPBJ5gg6zy4UxmQCL4jnIRAl7UdOHcg8sznouIAvjQQcqYsumuAnIrBxwdyJg008kYLx08z")
  ],
  providers: [PaymentMgmtService]
})
export class PaymentManagementModule {}
