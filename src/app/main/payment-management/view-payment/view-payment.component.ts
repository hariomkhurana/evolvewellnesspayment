import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

import { StripeService, StripeCardComponent } from "ngx-stripe";
import { StripeCardElementOptions, StripeElementsOptions } from "@stripe/stripe-js";

@Component({
  selector: "app-view-payment",
  templateUrl: "./view-payment.component.html",
  styleUrls: ["./view-payment.component.css"]
})
export class ViewPaymentComponent implements OnInit {
  @ViewChild(StripeCardComponent, null) card: StripeCardComponent;
  cardErrorMessage:string = "";
  planList:[
  {
    amount: 1500,
    currency: "inr",
    description: null,
    interval: "month",
    productName: "testProduct786",
    stripePriceId: "price_1ILi9ZBIVTngAwh3PrX8UP0x",
    stripeProductId: "prod_IxdQc7Y91RRfWa"

  },
  {
    amount: 150,
    currency: "inr",
    description: null,
    interval: "month",
    productName: "testProduct782",
    stripePriceId: "price_1ILi9ZBIVTngAwh3PrX8UP0x",
    stripeProductId: "prod_IxdQc7Y91RRfWa"

  },
  {
    amount: 150,
    currency: "inr",
    description: null,
    interval: "month",
    productName: "testProduct782",
    stripePriceId: "price_1ILi9ZBIVTngAwh3PrX8UP0x",
    stripeProductId: "prod_IxdQc7Y91RRfWa"

  }
]
  cardOptions: StripeCardElementOptions = {
    style: {
      base: {
        iconColor: "#666EE8",
        color: "#31325F",
        fontWeight: "300",
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSize: "18px",
        "::placeholder": {
          color: "#CFD7E0"
        }
      }
    }
  };

  elementsOptions: StripeElementsOptions = {
    locale: "en"
  };

  stripeForm: FormGroup;

  constructor(private fb: FormBuilder, private stripeService: StripeService) {}

  ngOnInit(): void {
    this.stripeForm = this.fb.group({
      name: ["", [Validators.required]]
    });
  }

  createToken(): void {
    this. cardErrorMessage= "";
    const name = this.stripeForm.get("name").value;
    let subRe = {
      cardToken: null,
      cardScheme:null,
      maskedCard:null,
      expiryMonth:null,
      expiryYear:null,
      nameOnCard : this.stripeForm.value.name 
    }
    this.stripeService.createToken(this.card.element, { name }).subscribe(result => {
      if (result.token) {
        // Use the token
        subRe.cardToken = result.token.id;;
        subRe.cardScheme = result.token.card.brand.toLocaleLowerCase();
        subRe.maskedCard = "XXXX-XXXX-XXXX-"+ result.token.card.last4;
        subRe.expiryMonth = result.token.card.exp_month;
        subRe.expiryYear = result.token.card.exp_year;
        let result1 =  subRe.cardScheme=='mastercard'
      ||  subRe.cardScheme=='visa'
      ||  subRe.cardScheme=='discover'
      ||  subRe.cardScheme =='american express';
      if (!result1) {
      subRe.cardScheme='other';
      }
      subRe['isDefault'] = true;
        console.log('finalRequst',subRe);
      } else if (result.error) {
        // Error creating the token
        this.cardErrorMessage = result.error.message;
        console.log(result.error.message);
      }
    });
  }
}