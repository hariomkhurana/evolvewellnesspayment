import { Injectable } from "@angular/core";
import { NetworkService } from "../../shared/network.service";

@Injectable()
export class PaymentMgmtService {
  constructor(private networkService: NetworkService) {}

  getChildProfileApi(query) {
    return this.networkService.get("api/child/?" + query, null, null, "bearer");
  }
  getSponsorProfileApi(query) {
    return this.networkService.get("api/sponsor/?" + query, null, null, "bearer");
  }

  updatecConsultantStatus(user: any) {
    return this.networkService.put("api/user/", user, null, "bearer");
  }

  uploadImage(image: any) {
    const formData = new FormData();

    formData.append("image", image, "image.jpg");
    return this.networkService.uploadImages("api/s3upload/image-upload", formData, null, "bearer");
  }

  postReportCardApi(body: any) {
    return this.networkService.post("api/report", body, null, "bearer");
  }

  postChildApi(body: any) {
    return this.networkService.post("api/child", body, null, "bearer");
  }
  putChildApi(body: any) {
    return this.networkService.put("api/child", body, null, "bearer");
  }
  assignSponsor(body: any) {
    return this.networkService.post("api/sponsorChild", body, null, "bearer");
  }

  viewReport(childId: any) {
    return this.networkService.get("api/report?childId=" + childId, null, null, "bearer");
  }

  deleteReport(reportId: any) {
    return this.networkService.delete("api/report/" + reportId, null, null, "bearer");
  }

  deleteChild(child: any) {
    return this.networkService.delete("api/child/" + child, null, null, "bearer");
  }
}
