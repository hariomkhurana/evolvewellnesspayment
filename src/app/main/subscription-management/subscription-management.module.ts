import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { SubscriptionManagementRoutingModule } from "./subscription-management-routing.module";
import { SubscriptionManagementComponent } from "./subscription-management.component";
import { ViewSubscriptionComponent } from "./view-subscription/view-subscription.component";
import { SharedModule } from "../../shared/shared.module";
import { SubscriptionMgmtService } from "./subscription.service";

@NgModule({
  declarations: [SubscriptionManagementComponent, ViewSubscriptionComponent],
  imports: [CommonModule, SubscriptionManagementRoutingModule, SharedModule],
  providers: [SubscriptionMgmtService]
})
export class SubscriptionManagementModule {}
