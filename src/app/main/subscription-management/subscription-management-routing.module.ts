import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { SubscriptionManagementComponent } from "../subscription-management/subscription-management.component";
import { ViewSubscriptionComponent } from "./view-subscription/view-subscription.component";

const routes: Routes = [
  {
    path: "",
    component: SubscriptionManagementComponent,
    children: [
      { path: "", redirectTo: "/main/subscription-management/view-subscription", pathMatch: "full" },
      { path: "view-subscription", component: ViewSubscriptionComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubscriptionManagementRoutingModule {}
