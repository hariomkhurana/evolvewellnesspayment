import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MainComponent } from "./main.component";
import { FullComponent } from "../layouts/full/full.component";
// import { DashboardComponent } from './dashboard/dashboard.component';

import { AuthGuardMain } from "../auth/auth-guard.service";
//import { FutureBookingComponent } from './booking-management/future-booking/future-booking.component';

const routes: Routes = [
  // { path: "", redirectTo: "/main/dashboard", pathMatch: "full" },
  {
    path: "main",
    component: MainComponent,
    canActivate: [AuthGuardMain],
    children: [
      { path: "", redirectTo: "/main/payment-management", pathMatch: "full" },

    
      {
        path: "payment-management",
        loadChildren: () =>
          import("../main/payment-management/payment-management.module").then(m => m.PaymentManagementModule)
      },
      {
        path: "subscription-management",
        loadChildren: () =>
          import("../main/subscription-management/subscription-management.module").then(
            m => m.SubscriptionManagementModule
          )
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule {}
